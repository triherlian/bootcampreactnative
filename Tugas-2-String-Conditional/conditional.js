const readline = require('../readline-sync');

// IF-ELSE
console.log("Soal If-Else");
var name = readline.question("Masukan nama kamu : ");
if (name == null || name == '') {
    console.log("Nama harus diisi!");
    return;
}

var peran = readline.question("Halo " + name + ", Masukan peran kamu : ");
if (peran == null || peran == '') {
    console.log("Peran harus diisi!");
    return;
}

var welcome = "Selamat datang di Dunia Werewolf, " + name;
if (peran.toLowerCase() === "penyihir") {
    console.log(welcome);
    console.log("Halo Penyihir " + name + ", kamu dapat melihat siapa yang menjadi werewolf!");
} else if (peran.toLowerCase() === "guard") {
    console.log(welcome);
    console.log("Halo Guard " + name + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (peran.toLowerCase() === "werewolf") {
    console.log(welcome);
    console.log("Halo Werewolf " + name + ", Kamu akan memakan mangsa setiap malam!");
} else {
    console.log("Peran tidak tersedia!");
    return;
}

// SWITCH
console.log("")
console.log("")
console.log("Soal Switch ")
var tahun = readline.question("Masukan tahun : ");
if(tahun < 1900 || tahun > 2200  ){
    console.log("Tahun tidak valid");
    return
}

var bulan = readline.question("Masukan bulan : ");
switch (bulan) {
    case "1":
        bulan = "Januari";
        break;
    case "2":
        bulan = "Februari";
        break;
    case "3":
        bulan = "Maret";
        break;
    case "4":
        bulan = "April";
        break;
    case "5":
        bulan = "Mei";
        break;
    case "6":
        bulan = "Juni";
        break;
    case "7":
        bulan = "Juli";
        break;
    case "8":
        bulan = "Agustus";
        break;
    case "9":
        bulan = "September";
        break;
    case "10":
        bulan = "Oktober";
        break;
    case "11":
        bulan = "November";
        break;
    case "12":
        bulan = "Desember";
        break;
    default:
        console.log("Bulan tidak valid");
        break;
}
var month31 = ["januari", "maret", "mei", "juli", "agustus", "oktober", "desember"];
var month30 = ["april", "juni", "september", "november"];
var month28 = ["februari"];

var tanggal = readline.question("Masukan tanggal : ");
switch (true) {
    case (month31.includes(bulan.toLowerCase()) && (tanggal >= 1 && tanggal <= 31)):
        console.log(tanggal + " " + bulan + " " + tahun);
        break;
    case (month30.includes(bulan.toLowerCase()) && (tanggal >= 1 && tanggal <= 30)):
        console.log(tanggal + " " + bulan + " " + tahun);
        break;
    case (month28.includes(bulan.toLowerCase()) && (((tanggal >= 1 && tanggal <= 29)) && (tahun % 4 == 0) || ((tanggal >= 1 && tanggal <= 28)))):
        console.log(tanggal + " " + bulan + " " + tahun);
        break;
    default:
        console.log("Tanggal tidak valid")
        return;
}
