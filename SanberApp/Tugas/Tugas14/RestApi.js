import axios from 'react-native-axios'
import React, { useState, useEffect } from 'react'
import { View, Button, FlatList, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native'

export default function RestApi() {
  const [title, setTitle] = useState("");
  const [value, setValue] = useState("");
  const [items, setItems] = useState([]);
  const [button, setButton] = useState("Simpan");
  const [selectedUser, setSelectedUser] = useState({})

  const submit = () => {
    const data = {
      value, title
    }

    if (button == "Simpan") {
      axios.post("https://achmadhilmy-sanbercode.my.id/api/v1/news", data)
        .then(res => {
          console.log('res: ', res)
          setTitle("")
          setValue("")
          GetData()
        }).catch(err => {
          console.log('error : ', err)
        })
    } else {
      axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selectedUser.id}`, data)
        .then(res => {
          console.log('res: ', res)
          setTitle("")
          setValue("")
          GetData()
          setButton("Simpan")
        }).catch(err => {
          console.log('error: ', err)
        })
    }
  }

  const onSelectItem = (item) => {
    console.log(item)
    setSelectedUser(item)
    setTitle(item.title)
    setValue(item.value)
    setButton("Update")
  }

  const GetData = () => {
    axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
      .then(res => {
        const data1 = (res.data.data)
        console.log('res: ', data1)
        setItems(data1)
      })
  }

  const onDelete = (item) => {
    axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
      .then(res => {
        console.log('res: ', res)
        GetData()
      }).catch(err => {
        console.log('error: ', err)
      })
  }

  useEffect(() => {
    GetData()
  }, [])


  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>Tampilkan Api (CRUD)</Text>
      </View>
      <View style={styles.content}>
        <Text style={{ marginBottom: 20, fontWeight: 'bold' }}>Form Data Berita</Text>

        <TextInput
          placeholder="Masukan judul berita"
          style={styles.input}
          value={title}
          onChangeText={(value) => setTitle(value)}
        />

        <TextInput
          placeholder="Masukan isi berita"
          style={styles.input}
          value={value}
          onChangeText={(value) => setValue(value)}
        />
        <Button
          title={button}
          onPress={submit}
        />
      </View>

      <View style={styles.content}>
        <Text style={{ marginBottom: 20, fontWeight: 'bold' }}>Get Data Berita</Text>
        <FlatList
          data={items}
          keyExtractor={(item, index) => `${item.id}-${index}`}
          renderItem={({ item }) => {
            return (
              <View>
                <TouchableOpacity onPress={() => onSelectItem(item)} style={styles.wrapperNews}>
                  <TouchableOpacity onPress={() => onDelete(item)} style={{ backgroundColor: 'white' }}>
                    <Text style={{ color: 'red', alignSelf: 'flex-end' }}>X</Text>
                  </TouchableOpacity>
                  <Text style={styles.titleNews}>{item.title}</Text>
                  <Text style={styles.contentNews}>{item.value}</Text>
                </TouchableOpacity>
              </View>
            )
          }}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f6f6',
    height: '100vh'
  },
  header: {
    paddingVertical: 32,
    paddingHorizontal: 16,
    backgroundColor: '#2a9dff',
    alignItems: 'center'
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20
  },
  content: {
    marginVertical: 8,
    marginHorizontal: 16,

    paddingVertical: 32,
    paddingHorizontal: 16,
    backgroundColor: 'white'
  },
  input: {
    borderWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 5,
    borderRadius: 6,
    marginBottom: 10
  },
  wrapperNews: {
    padding: 16,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    borderWidth: 1,
    borderColor: 'lightgrey',
    marginBottom: 8
  },
  titleNews: {
    paddingVertical: 10,
    color: 'black',
    fontWeight: 'bold'
  },
  contentNews: {
    paddingVertical: 10
  }
})