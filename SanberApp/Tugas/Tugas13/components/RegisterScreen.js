import React, { Component } from 'react'
import { View, StyleSheet, Image, TextInput, Text, Button, TouchableOpacity } from 'react-native'

export default class RegisterScreen extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../images/logo.png')} style={{ width:'100%',height:78 }}></Image>

                <View style={styles.titleWrapper}>
                <Text style={styles.registerText}>{'Register'}</Text>
                </View>

                <View style={styles.labelWrapper}>
                    <Text style={styles.textLabel}>{'Username'}</Text>
                    <TextInput style={styles.input}
                        underlineColorAndroid="transparent"
                        placeholder="Input username disini"
                        placeholderTextColor="#9aa3ba"
                        autoCapitalize="none" />
                </View>
                <View style={styles.labelWrapper}>
                    <Text style={styles.textLabel}>{'Email'}</Text>
                    <TextInput style={styles.input}
                        underlineColorAndroid="transparent"
                        placeholder="Input email disini"
                        placeholderTextColor="#9aa3ba"
                        autoCapitalize="none" />
                </View>
                <View style={styles.labelWrapper}>
                    <Text style={styles.textLabel}>{'Password'}</Text>
                    <TextInput style={styles.input}
                        underlineColorAndroid="transparent"
                        placeholder="Input password disini"
                        placeholderTextColor="#9aa3ba"
                        autoCapitalize="none" />
                </View>
                <View style={styles.labelWrapper}>
                    <Text style={styles.textLabel}>{'Ulang password'}</Text>
                    <TextInput style={styles.input}
                        underlineColorAndroid="transparent"
                        placeholder="Input Konfirmasi Password Disini"
                        placeholderTextColor="#9aa3ba"
                        autoCapitalize="none" />
                </View>

                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity style={styles.buttonDaftar}>
                        <Text style={{ color: '#ffffff' }}>Daftar</Text>
                    </TouchableOpacity >
                    <Text style={styles.textLabel}>{'Atau'}</Text>
                    <TouchableOpacity style={styles.buttonMasuk}>
                        <Text style={{ color: '#ffffff' }}>Masuk?</Text>
                    </TouchableOpacity >
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        padding: 15
    },

    registerText: {
        paddingTop: 30,
        paddingBottom: 30,
        color: '#003366',
        fontSize: 18
    },
    header: {
        alignItems: 'center'
    },
    titleWrapper: {
        alignSelf: 'stretch',
        alignItems:'center',
        flexDirection: 'column',
        marginBottom: 8
    },
    labelWrapper: {
        alignSelf: 'stretch',
        flexDirection: 'column',
        marginBottom: 8
    },
    textLabel: {
        color: '#003366',
        fontSize: 14,
        marginBottom: 4
    },
    input: {
        height: 40,
        padding: 8,
        alignItems: 'stretch',
        borderColor: '#3EC6FF',
        borderWidth: 1
    },
    buttonDaftar: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,
        backgroundColor: '#003366',
    },
    buttonMasuk: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,
        backgroundColor: '#3EC6FF',
    }
})