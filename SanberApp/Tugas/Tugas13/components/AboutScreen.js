import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class AboutScreen extends Component {
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <Text style={styles.titleText}>{'Tentang Saya'}</Text>
                    <Image style={styles.fotoProfil} source={require('../images/fotoprofil.png')}></Image>

                    <View style={{ marginBottom: 24, alignItems:'center' }}>
                        <Text style={styles.userName}>{'Kurniawan'}</Text>
                        <Text style={styles.userTitle}>{'React Native Developer'}</Text>
                    </View>

                    <View style={styles.wrapperSection}>
                        <View style={styles.titleSection}>
                            <Text>Portofolio</Text>
                        </View>

                        <View style={styles.wrapperSectionContent1}>
                            <View style={{ alignItems: 'center' }}>
                                <Icon name="gitlab" style={styles.iconItem} size={40} />
                                <Text style={styles.labelItem}>@Kurniawan</Text>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Icon name="github" style={styles.iconItem} size={40} />
                                <Text style={styles.labelItem}>@Kurniawan</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.wrapperSection}>
                    <View style={styles.titleSection}>
                        <Text>Hubungi Saya</Text>
                    </View>

                    <View style={styles.wrapperSectionContent2}>
                        <View style={styles.socialMediaContent}>
                            <Icon name="facebook" style={styles.iconItem} size={40} />
                            <Text style={styles.labelItem}>kurni.awan</Text>
                        </View>
                        <View style={styles.socialMediaContent}>
                            <Icon name="instagram" style={styles.iconItem} size={40} />
                            <Text style={styles.labelItem}>@kurni_awan</Text>
                        </View>
                        <View style={styles.socialMediaContent}>
                            <Icon name="twitter" style={styles.iconItem} size={40} />
                            <Text style={styles.labelItem}>@Kurniawn</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
    },

    userName: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    userTitle: {
        fontSize: 14,
        color: '#3EC6FF',
        fontWeight: 'bold'
    },

    fotoProfil: { width: 150, height: 150, borderRadius: 75 },
    wrapperSection: { backgroundColor: '#efefef', marginBottom: 12, alignSelf: 'stretch', padding: 12 },
    titleSection: { marginBottom: 12, borderBottomColor: '#003366', borderBottomWidth: 1 },
    wrapperSectionContent1: { flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'space-around', },
    wrapperSectionContent2: { flexDirection: 'column', alignSelf: 'stretch', justifyContent: 'space-around', paddingHorizontal: 90 },
    socialMediaContent: { alignItems: 'flex-start', flexDirection: 'row', alignItems: 'center' },

    titleText: {
        paddingTop: 30,
        paddingBottom: 30,
        color: '#003366',
        fontWeight:'bold',
        fontSize: 28
    },

    iconItem:{
        color:'#3EC6FF'
    },
    labelItem:{
        color:'#003366',
        fontWeight:'bold'
    }
})