import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default function Login({ navigation }) {
    return (
        <View style={styles.container}>
            <Text>Login</Text>
            <Button title="Menuju Halaman Homescreen" onPress={() => navigation.navigate("MyDrawwer", {
                screen: "App", params: {
                    screen: "AboutScreen"
                }
            })} />
        </View>
    )
}

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        }
    }
)
