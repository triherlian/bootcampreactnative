import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Telegram from './Tugas/Tugas12/Telegram';
import Tugas13 from './Tugas/Tugas13/App';
import RestApi from './Tugas/Tugas14/RestApi';
import Tugas15 from './Tugas/Tugas15/Router/index';
import Quiz3 from './Tugas/Quiz3/index';

export default function App() {
  return (
    <View style={styles.container}>
      {/* <Telegram></Telegram> */}
      {/* <Tugas13></Tugas13> */}
      {/* <RestApi></RestApi> */}
      {/* <Tugas15></Tugas15> */}
      <Quiz3></Quiz3>
    </View>
  );
}

const styles = StyleSheet.create({
  container:{
    height:'100vh'
  }
})