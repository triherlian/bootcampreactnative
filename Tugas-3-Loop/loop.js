// SOAL 1 WHILE
console.log("Soal 1 looping while");
console.log("")
var index = 2;
console.log("LOOPING PERTAMA")
while(index <= 20){
    console.log(index + " - I love coding");
    index+=2;
}
console.log("LOOPING KEDUA")
while(index > 2){
    index-=2;
    console.log(index + " - I will become a mobile developer");
}

// SOAL 2 FOR
console.log("")
console.log("")
console.log("Soal 2 Looping menggunakan for")
for(let i = 1; i<=20;i++){ 
    if( (i%2==1) && (i%3==0) ){
        console.log(i + " - I Love Coding");
    }else if(i%2==1){
        console.log(i + " - Santai");
    }else if(i%2==0){
        console.log(i + " - Berkualitas");
    }
}

// SOAL 3
var x,y,limitX,limitY,hastag;

console.log("")
console.log("")
console.log("Soal 3 Membuat Persegi Panjang #")
x=0;
y=0;
limitX=8;
limitY=4
hastag="";
while(y<limitY){
    while(x<limitX){
        hastag+="#";
        x++;
    }
    console.log(hastag);
    hastag = ""
    x=0;
    y++;
}

// SOAL 4
console.log("")
console.log("")
console.log("Soal 4 Membuat Tangga")
x=0;
y=0;
limitX=1;
limitY=7
hastag="";
while(y<limitY){
    while(x<limitX){
        hastag+="#";
        x++;
    }
    console.log(hastag);
    hastag = ""
    limitX++;
    x=0;
    y++;
}

// SOAL 5
console.log("")
console.log("")
console.log("Soal 5 Membuat Tangga")
x=0;
y=0;
limitX=8;
limitY=8
hastag="";
while(y<limitY){
    while(x<limitX){
        if( ( (y+1)%2 == 0) ){
            if( (x+1)%2 == 0 ){
                hastag+=" ";
            }else{
                hastag+="#";
            }
        }else{
            if( (x+1)%2 == 0 ){
                hastag+="#";
            }else{
                hastag+=" ";
            }
        }
        x++;
    }
    console.log(hastag);
    hastag = ""
    x=0;
    y++;
}