class Animal {
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }

    get name() {
        return this._name;
    }
    set name(x) {
        this._name = x;
    }

    get legs() {
        return this._legs;
    }
    set legs(x) {
        this._legs = x;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }
    set cold_blooded(x) {
        this._cold_blooded = x;
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name)
        this._sound = "Auooo"
    }
    
    get sound(){
        return this._sound
    }
    set sound(x) {
        this._sound = x;
    }

    yell() {
        console.log(this.sound)
    }
}
class Frog extends Animal {
    
    constructor(name) {
        super(name)
        this._sound = "hop hop"
    }
    
    get sound(){
        return this._sound
    }
    set sound(x) {
        this._sound = x;
    }
    jump() {
        console.log(this.sound)
    }
}

console.log("1. Animal Class ")
console.log("Release 0")

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


console.log("")
console.log("Release 1")
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 




console.log("")
console.log("")
console.log("2. Function to Class")

class Clock {
    // Code di sini
    constructor(obj){
        this._template = obj.template
        this._timer = null
    }

    get template(){
        return this._template
    }
    set template(x){
        this._template = x
    }
    get timer(){
        return this._timer
    }
    set timer(x){
        this._timer = x
    }

    render(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
    }

    stop(){
        clearInterval(this.timer)
    }

    start(){
        this.render()
        this.timer = setInterval(()=>{
            this.render()
        }, 1000)
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 