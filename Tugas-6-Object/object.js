console.log("Soal No 1 (Array to Object)")
function arrayToObject(arr) {
    var indexFirstName = 0
    var indexLastName = 1
    var indexGender = 2
    var indexYear = 3
    if (arr.length > 0) {
        var object = new Object();
        for (let a1 of arr) {
            var object2 = new Object();
            object2["firstName"] = a1[indexFirstName] != null ? a1[indexFirstName] : "Invalid First Name"
            object2["lastName"] = a1[indexLastName] != null ? a1[indexLastName] : "Invalid Last Name"
            object2["gender"] = a1[indexGender] != null ? a1[indexGender] : "Invalid Gender"
            object2["age"] = a1[indexYear] != null && a1[indexYear] <= new Date().getFullYear() ? new Date().getFullYear() - a1[indexYear] : "Invalid Birth Year"

            object[a1[indexFirstName] + " " + a1[indexLastName]] = object2
            console.log(object)
            object = new Object();
        }
    } else {
        console.log("")
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
arrayToObject([])

console.log("")
console.log("")
console.log("Soal No 2 (Shopping Time)")

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var catalog = [{ name: "Sepatu Stacattu", price: 1500000 }, { name: "Baju Zoro", price: 500000 }, { name: "Baju H&N", price: 250000 }, { name: "Sweater Uniklooh", price: 175000 }, { name: "Casing Handphone", price: 50000 }]
    var shoppingCart = [
        {
            memberId: '1820RzKrnWn08',
            listPurchased: [
                'Sepatu Stacattu',
                'Baju Zoro',
                'Baju H&N',
                'Sweater Uniklooh',
                'Casing Handphone'
            ]
        },
        {
            memberId: '82Ku8Ma742',
            listPurchased: [
                'Casing Handphone'
            ]
        },
        {
            memberId: '234JdhweRxa53',
            listPurchased: []
        },
    ]

    let indexSC = shoppingCart.findIndex(sc => sc.memberId == memberId);
    if (indexSC > -1) {
        catalog.sort((a, b) => (a.price - b.price))
        if (catalog[0].price < money) {
            var object = new Object();
            var cart = []
            var total = 0

            for (let sc of shoppingCart[indexSC].listPurchased) {
                let indexCatalog = catalog.findIndex(c => c.name == sc)
                total += catalog[indexCatalog].price
                cart.push(catalog[indexCatalog])
            }

            object["memberId"] = memberId
            object["money"] = money
            object["listPurchased"] = cart
            object["changeMoney"] = money - total

            return object
        } else {
            return "Mohon maaf, uang tidak cukup"
        }
    } else {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("")
console.log("")
console.log("Soal No. 3 (Naik Angkot)")

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here

    var indexNama = 0;
    var indexFrom = 1;
    var indexTo = 2;

    var objects = []
    for(let p of arrPenumpang){
        var indexRuteFrom = rute.findIndex(r => r == p[indexFrom]);
        var indexRuteTo = rute.findIndex(r => r == p[indexTo]);

        var totalBayar = 0
        for(let i = indexRuteFrom; i<indexRuteTo; i++){
            totalBayar += 2000
        }

        var object = {
            penumpang:p[indexNama],
            naikDari:p[indexFrom],
            tujuan:p[indexTo],
            bayar:totalBayar
        }

        objects.push(object)
    }

    return objects
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]